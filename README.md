# Teleport Webassets with Disable user change password

- git clone https://github.com/gravitational/teleport.git -b v10.0.2
- edit .gitmodules file
- https://github.com/gravitational/webassets.git -> https://gitlab.com/nhnqui/webassets.git
- install golang-go
- make full
- copy build/teleport to /usr/local/bin/teleport 